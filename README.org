* treeclim

An R package for modeling tree/climate relationships.

** Installation

On Windows, you need [[http://cran.r-project.org/bin/windows/Rtools/][Rtools]] to be installed to install the package
from source (or install a [[https://github.com/znag/climtree/releases][binary]]). Then do:

#+begin_src R 
library(devtools)
install_github("treeclim", "znag")
#+end_src

** Usage

see the [[https://github.com/znag/treeclim/wiki][Wiki]] for details.
